LO41 Project Preempting Death Star Lift To The Highest Ground
=============================================================
Pierre GURIEL--FARDEL, Yann LE CHEVANTON

Installation
------------

1/ Clone the repository (or download it, idc)
```
git clone https://gitlab.com/pyguri/UTBM_LO41_Preempting_Death_Star_Lift_To_The_Highest_Ground.git ~/Desktop/Best_LO41_Project
```

2/ Generate the makefile using cmake
```
cmake .
```

3/ Build the executable
```
make all
```

4/ Run the program with or without parameters (without, the default parameters will be used)
```
./Preempting_Death_Star_Lift_To_The_Highest_Ground <nb_floor> <nb_lift> <nb_client>
```