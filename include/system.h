//
// Created by davion on 13/06/18.
//

#ifndef UTBM_LO41_PREEMPTING_DEATH_STAR_LIFT_TO_THE_HIGHEST_GROUND_SYSTEM_H
#define UTBM_LO41_PREEMPTING_DEATH_STAR_LIFT_TO_THE_HIGHEST_GROUND_SYSTEM_H

#include <pthread.h>
#include <stdbool.h>
#include <signal.h>

#include "./lift.h"
#include "./client.h"

/** @struct The system managing acting as environment for the threads, storing the lifts, requests and some gobals
 * @param nb_lift           number of lifts
 * @param max_request       maximum number of client requests stored
 * @param lifts             lifts managed by the master ; array
 * @param requests          clients requesting moving from the LiftMaster ; stored as pointers ; array
 * @Monitor
 * @param TIDs              the array containing all of the thread from the program, stores the lifts first and the the clients
 * @param mutex_cond_floor  the mutices used to lock 'cond_floor' ; array
 * @param mutex_cond_lift   the mutices used to lock 'cond_lift' ; 2 dimensional array 'mutex_cond_lift[nb_lift][nb_floor]'
 * @param mutex_lift        the mutices used to lock the critical data in each lift ; array
 * @param mutex_master      the mutex used to lock the critical data in the system
 * @param cond_floor        the conditions used to wait at a each floor ; array
 * @param cond_lift         the conditions used to wait in a certain lift for a certain floor ; 2 dimensional array 'cond_lift[nb_lift][nb_floor]'
*/
typedef struct _System {
    int nb_floor;
    int nb_lift;
    int nb_client;
    bool init;
    Lift *lifts;
    Client **requests;
    pthread_t *TIDs;
    pthread_mutex_t *mutex_cond_floor;
    pthread_mutex_t **mutex_cond_lift;
    pthread_mutex_t *mutex_lift;
    pthread_mutex_t mutex_master;
    pthread_cond_t *cond_floor;
    pthread_cond_t **cond_lift;

} System;

/** Function used to declare the (singleton) system, if it is already declared, returns the one actually in use
 * @return The 'System' instance of the program
 */
System *get_system();

/** @implements Create a (singleton) system based on the number of lift and clients
 * @param nb_floor      number of floor in the system
 * @param nb_lift       number of lift the master will manage
 * @param max_request   maximum number of client requests the master can take
 * @return              the system with the right number of (default) lift and request capacity
*/
System* generate_system(int nb_floor, int nb_lift, int nb_client);

/** Signal handler for exiting a thread
 * @param sig the signal caught
 */
void int_thread(int sig);

/** @implements Free the allocated arrays, mutices and cancel the threads in the (singleton) system
*/
void destroy_system();

/** @implements Start all the thread related to the lift
*/
void start_lifts();

#endif //UTBM_LO41_PREEMPTING_DEATH_STAR_LIFT_TO_THE_HIGHEST_GROUND_SYSTEM_H
