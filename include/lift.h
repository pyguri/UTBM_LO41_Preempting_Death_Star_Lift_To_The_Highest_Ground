//
// Created by davion on 01/06/18.
//

#ifndef UTBM_LO41_PREEMPTING_DEATH_STAR_LIFT_TO_THE_HIGHEST_GROUND_LIFT_H
#define UTBM_LO41_PREEMPTING_DEATH_STAR_LIFT_TO_THE_HIGHEST_GROUND_LIFT_H

/* The capacity of a lift */
#define LIFT_CAPACITY 10

/* The time of travel from one floor to another in a lift*/
struct timespec travel_time;

/** @struct The lift used to move around clients, each lift is a thread
 * @param id            each lift has an id to differentiate it from the others
 * @param load          how many people in the lift
 * @param floor         position
 * @param destination   destination of the lift
 * @param direction     direction of the lift
 * @param capacity      load is capped to a maximum capacity
 * @param exit          the boolean used by the system to tell to the thread to finish
*/
typedef struct _Lift {
    int id;                  
    int load;               
    int floor;               
    int destination;
    bool exit;
} Lift;

/** @implements Create a default lift
 * @param id    id of the lift
 * @return      the default lift, load = 0, floor = 0, destination = 0
*/
Lift generate_lift(int id);

/** @implements Give the direction of the lift based on its destination
 * @param lift  the lift we want to know the direction of
 * @return      the direction of 'lift', +1 : up, -1 : down, 0 : stopped
*/
int lift_direction_comp(Lift *lift);

/** @implements Make the given lift move to a given floor,
 * @param lift  lift we want to move
 * @param floor floor we want to go to
*/
void lift_move_to(Lift *lift, int floor);

/** @implements Make the decision for the given lift on where it has to go
 * @param lift      lift we want to move
 * @return          the floor 'lift' has to go to
*/
int next_move(Lift *lift);

/** @implements Print all clients inside the lift
 * @param master    system that manages the lift
 * @param lift      considered lift
*/
void print_clients_inside(Lift* lift);

/** @implements How a lift is supposed to act according to it's environment, thread
 * @param lift      the lift that acts on himself
 * @param master    the system used to represent the environment
*/
void * lift_act(void *lift);

#endif //UTBM_LO41_PREEMPTING_DEATH_STAR_LIFT_TO_THE_HIGHEST_GROUND_LIFT_H
