//
// Created by davion on 01/06/18.
//

#ifndef UTBM_LO41_PREEMPTING_DEATH_STAR_LIFT_TO_THE_HIGHEST_GROUND_CLIENT_H
#define UTBM_LO41_PREEMPTING_DEATH_STAR_LIFT_TO_THE_HIGHEST_GROUND_CLIENT_H

#include "lift.h"

/** @struct The client using the lifts to move from one floor to another, each client is a thread
 * @param start         floor where the client starts
 * @param destination   floor where the client wants to go to
 * @param lift          lift the client is in, if not in a lift -> NULL
*/
typedef struct _Client {
    int start;
    int destination;
    int id;
    Lift *lift;
} Client;

/** @implements Create a client with given floor and destination
 * @param start         starting floor of the client
 * @param destination   destination of the client
 * @return              the client with the right floor and destination outside of a lift ('client.lift' = NULL)
*/
Client generate_client(int id, int start, int destination);

/** @implements Client states that the is waiting for a lift --> adds himself to the 'requests' array
 * @param client    the client making the request
 * @param master    the system 'client' is making a request to
*/
void client_request(Client *client);

/** @implements Client has been served --> removes himself from the 'master.requests' array
 * @param client the client that finished his/her trip
 * @param master the system that served 'client'
*/
void client_served(Client *client);

/** @implements Find the direction where the client wants to go
 * @param client    the client we want to know the direction of
 * @return          the direction of 'client', +1 : up, -1 : down
*/
int client_direction(Client client);

/** @implements Client enters lift
 * @param client    the client that want to enter 'lift'
 * @param lift      le lift 'client' wants to enter in
 * @return          the success or the failure to enter
*/
int client_enter_lift(Client *client, Lift *lift);

/** @implements Client exits lift */
int client_exit_lift(Client *client, Lift *lift);

/** @implements The client act accordingly to its environment, thread
 * @param client    the client that acts on himself
 * @param master    the system used to represent the environment
*/
void * client_act(void *client);

#endif //UTBM_LO41_PREEMPTING_DEATH_STAR_LIFT_TO_THE_HIGHEST_GROUND_CLIENT_H
