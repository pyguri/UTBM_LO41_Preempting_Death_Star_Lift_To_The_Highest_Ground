//
// Created by davion on 02/06/18.
//

#include <stdlib.h>
#include <stdio.h>
#include "../include/system.h"

Client generate_client(int id, int start, int destination) {
    Client result;

    result.start = start;
    result.destination = destination;
    result.id = id;
    result.lift = NULL;
    return result;
}

void client_request(Client *client) {
    System *sys = get_system();
    pthread_mutex_lock(&sys->mutex_master);
    int i = 0;
    while (i < sys->nb_client && sys->requests[i] != NULL && sys->requests[i]->id != client->id) {
        ++i;
    }

    if (i < sys->nb_client)
        sys->requests[i] = client;

    pthread_mutex_unlock(&sys->mutex_master);
}

void client_served(Client *client) {
    System *sys = get_system();
    pthread_mutex_lock(&sys->mutex_master);
    int i = 0;
    while (i < sys->nb_client && sys->requests[i] != client) {
        ++i;
    }

    if (i < sys->nb_client) {
        sys->requests[i] = NULL;
    }
    pthread_mutex_unlock(&sys->mutex_master);
    printf("Goodbye %i !\n",client->id);
}

int client_direction(Client client) {
    return ((client.destination - client.start) / (abs(client.destination - client.start)));
}

int client_enter_lift(Client *client, Lift *lift) {
    int out;
    System *sys = get_system();
    pthread_mutex_lock(&sys->mutex_lift[lift->id]);
    if (lift->floor != client->start) {
        printf("Client at start %d tried entering lift %d at floor %d \n", client->start, lift->id, lift->floor);
        out = EXIT_FAILURE;
    } else {
        if (lift->load < LIFT_CAPACITY) {
            client->lift = lift;
            lift->load++;
            out = EXIT_SUCCESS;
        } else {
            printf("Lift full\n");
            out = EXIT_FAILURE;
        }
    }
    pthread_mutex_unlock(&sys->mutex_lift[lift->id]);
    return out;
}

int client_exit_lift(Client *client, Lift *lift) {
    int out;
    System *sys = get_system();
    pthread_mutex_lock(&sys->mutex_lift[lift->id]);
    if (lift != client->lift) {
        printf("Client in lift %d tried exiting lift %d \n", client->lift->id, lift->id);
        out = EXIT_FAILURE;
    } else {
        lift->load--;
        client->lift = NULL;
        out = EXIT_SUCCESS;
    }
    pthread_mutex_unlock(&sys->mutex_lift[lift->id]);
    return out;
}

void *client_act(void *_client) {
    sigset_t set;
    sigemptyset(&set);
    sigaddset(&set, SIGINT);
    pthread_sigmask(SIG_BLOCK, &set, NULL);
    signal(SIGUSR1, int_thread);

    Client *client = (Client *) _client;
    System *sys = get_system();
    client_request(client);
    while (client->lift == NULL || client->lift->floor != client->destination) {
        printf("Client %i waiting at %i\n", client->id, client->start);
        pthread_cond_wait(&sys->cond_floor[client->start],
                          &sys->mutex_cond_floor[client->start]);

        int i = 0;
        while (i < sys->nb_lift && sys->lifts[i].floor != client->start && sys->lifts[i].load < LIFT_CAPACITY)
            i++;

        if (/*(sys->lifts[i].direction == 0 || sys->lifts[i].direction == client_direction(*client)) &&*/
            client_enter_lift(client, &sys->lifts[i]) == EXIT_SUCCESS) {
            printf("Clients %i : Entering lift %i\n",client->id, client->lift->id);
            pthread_mutex_unlock(&sys->mutex_cond_floor[client->start]);
            pthread_cond_signal(&sys->cond_floor[client->start]);        // wake up the next client to enter, failing that, the lift
            pthread_cond_wait(&sys->cond_lift[client->lift->id][client->destination],
                              &sys->mutex_cond_lift[client->lift->id][client->destination]);
        }
    }
    printf("Client %i : Exiting lift %i\n",client->id, client->lift->id);
    pthread_mutex_unlock(&sys->mutex_cond_lift[client->lift->id][client->destination]);
    pthread_cond_signal(&sys->cond_lift[client->lift->id][client->destination]);     // wake up the next client to exit, failing that, the lift
    client_exit_lift(client, client->lift);
    client_served(client);
    pthread_exit(NULL);
}