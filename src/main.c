//
// Created by davion on 26/05/18.
//

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "../include/system.h"

/** Signal handler used to catch SIGINT
 * @param sig the signal caught
*/
void sig_handler(int sig) {
    if (sig == SIGINT) {
        printf("SIGINT caught\n");
        destroy_system();
        pthread_exit(NULL);
    }
}

/** The main function, used to run the program
 * @param argc  The number of arguments (counting the call of the program), this should be 4
 * @param argv  The arguments used by the program, these should be in order 'nb_floor' 'nb_lift' 'nb_client'
 * @return      The success or not of the program
*/
int main(int argc, char *argv[]) {
    printf("LO41 Lift Project\n");
    int nb_floor,
            nb_lift,
            nb_client;

    if (argc == 4) {
        nb_floor = (int) strtol(argv[1], NULL, 10);
        nb_lift = (int) strtol(argv[2], NULL, 10);
        nb_client = (int) strtol(argv[3], NULL, 10);
    } else {
        printf("Bad parameters (main(nb_floor, nb_lift, nb_client)), using default setup\n");
        nb_client = 500;
        nb_floor = 25;
        nb_lift = 3;
    }

    System *ObiWan = generate_system(nb_floor, nb_lift, nb_client);
    start_lifts();
    signal(SIGINT, sig_handler);

    int i = 0,
        spawn = 0,
        start, destination;

    srand(42);                  // randomizing seed TODO: set it as the clock for real use
    Client clients[nb_client];
    while (i < nb_client) {     // generating random spawn of client with random floor attributes
        if (spawn >= 100) {
            start = rand() % nb_floor;
            destination = rand() % nb_floor;
            while (destination == start)
                destination = rand() % nb_floor;

            clients[i] = generate_client(i, start, destination);
            pthread_create(&ObiWan->TIDs[nb_lift + i], NULL, client_act, &clients[i]);
            spawn = 0;
            i++;
        } else
            spawn += rand() % 100;
        nanosleep(&travel_time, NULL);
    }

    int nb_threads = nb_client + nb_lift;
    for (int j = nb_lift; j < nb_threads; ++j) {        // wait for all client to finish
        pthread_join(ObiWan->TIDs[j], NULL);
    }

    printf("It's over <%i clients>, I have the high ground\n", nb_client);
    destroy_system();
    return EXIT_SUCCESS;
}