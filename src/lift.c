//
// Created by davion on 02/06/18.
//

#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <stdio.h>
#include "../include/system.h"

Lift generate_lift(int id) {
    Lift result;

    result.id = id;
    result.load = 0;
    result.floor = 0;
    result.destination = 0;
    result.exit = false;

    return result;
}

int lift_direction_comp(Lift *lift) {
    int result = (int) 0;
    if (lift->destination != lift->floor)
        result = ((lift->destination - lift->floor) / (abs(lift->destination - lift->floor)));

    return result;
}

void lift_move_to(Lift *lift, int floor) {
    System *system = get_system();
    pthread_mutex_lock(&system->mutex_lift[lift->id]);
    lift->destination = floor;
    nanosleep(&travel_time, NULL);             // time to move from one floor to another
    lift->floor += lift_direction_comp(lift);;
    pthread_mutex_unlock(&system->mutex_lift[lift->id]);
}

int next_move(Lift *lift) {
    System *sys = get_system();
    pthread_mutex_lock(&sys->mutex_master);
    pthread_mutex_lock(&sys->mutex_lift[lift->id]);
    int distance = (int) INFINITY,
            choice = (int) NAN;
    bool already_chosen;
    Client *client;


    if (lift_direction_comp(lift) == 0) {
        /* 'lift' is at its destination,
         * we look for the nearest destination in our clients
         * we look for the nearest client waiting for a lift and in the same direction */
        for (int i = 0; i < sys->nb_client; ++i) {
            client = sys->requests[i];

            if (client != NULL &&
                client->lift == lift &&
                abs(client->destination - lift->floor) <= distance) {
                /* client waiting inside */
                distance = abs(client->destination - lift->floor);
                choice = client->destination;
            }
        }

        if (lift->load < LIFT_CAPACITY) {
            for (int i = 0; i < sys->nb_client; ++i) {
                client = sys->requests[i];

                if (client != NULL &&
                    client->lift == NULL &&
                    abs(client->start - lift->floor) <= distance) {
                    /* client waiting outside */
                    if (choice != NAN &&
                        (choice - lift->floor) / abs(choice - lift->floor) == client_direction(*client)) {
                        /* in the same direction */

                        already_chosen = false;
                        for (int j = 0; j < sys->nb_lift - 1; j++)
                            if (choice == sys->lifts[i].destination)
                                /* a lift is already going for this floor */
                                already_chosen = true;

                        if (already_chosen == false) {
                            /* no lift is going for this floor */
                            distance = abs(client->start - lift->floor);
                            choice = client->start;
                        }

//                        distance = abs(client->start - lift->floor);
//                        choice = client->start;
                    }
                    if (lift->load == 0) {
                        /* no client in lift */
                        distance = abs(client->start - lift->floor);
                        choice = client->start;
                    }
                }
            }
        }

/* 'lift' already has a direction it keeps going */
    } else choice = lift->destination;

    if (choice == NAN)
        choice = lift->floor;
    pthread_mutex_unlock(&sys->mutex_master);
    pthread_mutex_unlock(&sys->mutex_lift[lift->id]);
    return choice;
}

void print_clients_inside(Lift *lift) {
    System *sys = get_system();
    pthread_mutex_lock(&sys->mutex_lift[lift->id]);
    pthread_mutex_lock(&sys->mutex_master);
    Client *client;
    printf("with");
    for (int i = 0; i < sys->nb_client; ++i) {
        client = sys->requests[i];;
        if (client != NULL &&
            client->lift == lift) {
            /* For each client inside */
            printf(" : %i", client->id);
        }
    }
    printf("\n");
    pthread_mutex_unlock(&sys->mutex_master);
    pthread_mutex_unlock(&sys->mutex_lift[lift->id]);
}

void *lift_act(void *_lift) {
    sigset_t set;
    sigemptyset(&set);
    sigaddset(&set, SIGINT);
    pthread_sigmask(SIG_BLOCK, &set, NULL);

    Lift *lift = (Lift *) _lift;
    System *sys = get_system();
    while (!lift->exit) {
        lift_move_to(lift, next_move(lift));
        if (lift->floor == lift->destination) {
            printf("Lift %i arrived at floor %i ", lift->id, lift->floor);
            print_clients_inside(lift);

            pthread_cond_signal(&sys->cond_lift[lift->id][lift->floor]);  // wake-up all clients in 'lift' waiting for the floor
            nanosleep(&travel_time, NULL);
//            if (there are clients in the lift)
//                pthread_cond_wait(&sys->cond_lift[lift->id][lift->floor],
//                                  &sys->mutex_cond_lift[lift->id][lift->floor]);

            pthread_cond_signal(&sys->cond_floor[lift->floor]);           // wake-up all client at the floor
            nanosleep(&travel_time, NULL);
//            if (there are client at the floor)
//                    pthread_cond_wait(&cond_, &mutex);
        }
    }
    pthread_exit(NULL);
}
