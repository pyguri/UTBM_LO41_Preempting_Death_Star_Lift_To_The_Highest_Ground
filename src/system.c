//
// Created by davion on 13/06/18.
//

#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>
#include <stdio.h>
#include "../include/system.h"

System *get_system() {
    static System *result = NULL;
    if (result == NULL){
        result = (System *) malloc(sizeof(System));
        result->init = false;
    }

    return result;
}

System *generate_system(int nb_floor, int nb_lift, int nb_client) {
    System *result = get_system();

    if (!result->init) {
        result->init = true;
        travel_time.tv_sec = 0;
        travel_time.tv_nsec = 600000000L;   // 0.6 seconds
        result->nb_floor = nb_floor;
        result->nb_lift = nb_lift;
        result->nb_client = nb_client;

        result->lifts = (Lift *) malloc(nb_lift * sizeof(Lift));
        result->requests = (Client **) malloc(nb_client * sizeof(Client));
        for (int l = 0; l < nb_client; ++l) {
            result->requests[l] = NULL;
        }

        /** setting up the monitor */
        int nb_threads = nb_client + nb_lift;
        /* allocating all the 1D arrays */
        result->TIDs = (pthread_t *) malloc(nb_threads * sizeof(pthread_t));
        result->mutex_cond_floor = (pthread_mutex_t *) malloc(nb_floor * sizeof(pthread_mutex_t));
        result->mutex_cond_lift = (pthread_mutex_t **) malloc(nb_lift * sizeof(pthread_mutex_t *));
        result->mutex_lift = (pthread_mutex_t *) malloc(nb_lift * sizeof(pthread_mutex_t));
        result->cond_floor = (pthread_cond_t *) malloc(nb_floor * sizeof(pthread_cond_t));
        result->cond_lift = (pthread_cond_t **) malloc(nb_lift * sizeof(pthread_cond_t *));

        /* allocating the 2D arrays */
        for (int i = 0; i < nb_lift; ++i) {
            result->cond_lift[i] = (pthread_cond_t *) malloc(nb_floor * sizeof(pthread_cond_t));
            result->mutex_cond_lift[i] = (pthread_mutex_t *) malloc(nb_floor * sizeof(pthread_mutex_t));
        }

        for (int j = 0; j < nb_floor; ++j) {
            pthread_mutex_init(&result->mutex_cond_floor[j], NULL);
            pthread_cond_init(&result->cond_floor[j], NULL);
        }

        for (int k = 0; k < nb_lift; ++k) {
            pthread_mutex_init(&result->mutex_lift[k], NULL);
            for (int i = 0; i < nb_floor; ++i) {
                pthread_mutex_init(&result->mutex_cond_lift[k][i], NULL);
                pthread_cond_init(&result->cond_lift[k][i], NULL);
            }
        }

        pthread_mutex_init(&result->mutex_master, NULL);

        for (int i = 0; i < result->nb_lift; ++i)
            result->lifts[i] = generate_lift(i);

        for (int j = 0; j < result->nb_client; ++j)
            result->requests[j] = NULL;
    }
    return result;
}

void int_thread(int sig){
    if (sig == SIGUSR1){
        printf("%i caught SIG, exiting\n", (int) pthread_self());
        pthread_exit(NULL);
    }
}

void destroy_system() {
    System *system = get_system();
    int nb_floor = system->nb_floor,
            nb_lift = system->nb_lift,
            nb_client = system->nb_client,
            nb_threads = nb_client + nb_lift;

    for (int k = 0; k < nb_lift; ++k)
        system->lifts[k].exit = true;

    for (int l = nb_lift; l < nb_threads; ++l)
        if (system->requests[l - nb_lift] != NULL)
            pthread_kill(system->TIDs[l], SIGUSR1);

    for (int n = 0; n < nb_threads; ++n)
        pthread_join(system->TIDs[n], NULL);


    for (int i = 0; i < nb_floor; ++i) {
        pthread_mutex_destroy(&system->mutex_cond_floor[i]);
        pthread_cond_destroy(&system->cond_floor[i]);
    }
    for (int j = 0; j < nb_lift; ++j) {
        pthread_mutex_destroy(&system->mutex_lift[j]);
        for (int i = 0; i < nb_floor; ++i) {
            pthread_mutex_destroy(&system->mutex_cond_lift[j][i]);
            pthread_cond_destroy(&system->cond_lift[j][i]);
        }
    }
    pthread_mutex_destroy(&system->mutex_master);

    for (int m = 0; m < nb_lift; ++m){
        free(system->mutex_cond_lift[m]);
        free(system->cond_lift[m]);
    }

    free(system->requests);
    free(system->lifts);
    free(system->TIDs);
    free(system->mutex_cond_lift);
    free(system->mutex_cond_floor);
    free(system->mutex_lift);
    free(system->cond_floor);
    free(system->cond_lift);

    free(system);
}

void start_lifts(){
    System *system = get_system();
    for (int i = 0; i < system->nb_lift; ++i)
        pthread_create(&system->TIDs[i], NULL, lift_act, &system->lifts[i]);

}